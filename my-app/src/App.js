import React from 'react';
import './App.css';
import Footer from "./Component/Footer";
import Navbar from "./Component/NavBar";
import {UserProvider} from "../src/User/user-context";

function App() {
  return (
    <>
      <UserProvider>
        <Navbar />
        <Footer />
      </UserProvider>
    </>
    );
  }

export default App;