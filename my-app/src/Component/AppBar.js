import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Welcome from '../Pages/Welcome';
import ListMovie from '../Pages/ListMovie';
import ListGame from '../Pages/ListGame';
import TabelMovie from '../Pages/TabelMovie';
import TabelGame from '../Pages/TabelGame';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={5}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function SimpleTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Tabs style={{overflow: "auto"}} value={value} onChange={handleChange} aria-label="simple tabs example">
          
          <Tab label="Beranda" {...a11yProps(0)} />
          <Tab label="List Movie" {...a11yProps(1)} />
          <Tab label="List Game" {...a11yProps(2)} />
          <Tab label="Tabel Movie" {...a11yProps(3)} />
          <Tab label="Tabel Game" {...a11yProps(4)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
      <ListMovie /> 
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Welcome />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <ListGame />
      </TabPanel>
      <TabPanel value={value} index={3}>
        <TabelMovie />
      </TabPanel>
      <TabPanel value={value} index={4}>
        <TabelGame />
      </TabPanel>
    </div>
  );
}