import React, {useContext} from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from 'react-router-dom';
import {  } from "../App.css";
import {} from "../style.css";
import Welcome from '../Pages/Welcome';
import ListMovie from '../Pages/ListMovie';
import ListGame from '../Pages/ListGame';
import TabelMovie from '../Pages/TabelMovie';
import TabelGame from '../Pages/TabelGame';
import Register from '../Pages/User';
import Login from '../Pages/UserLogin';
import {useHistory} from "react-router-dom"
import {UserContext} from "../User/user-context";


const Navbar = () =>{
    let history = useHistory()
    const [user, setUser] = useContext(UserContext)
    const showMenu = () => {
        var menu = document.getElementById("navigasi")
        if (menu.style.display !== "block"){
            menu.style.display = "block";
        } else {
            menu.style.display = "none"
        }
    }
    
    const handleLogout = () =>{
        
      setUser(null)
      localStorage.removeItem("user")
      var register = document.getElementById("register")
      var login = document.getElementById("login")
      var logout = document.getElementById("logout")
      var tmovie = document.getElementById("tmovie")
      var tgame = document.getElementById('tgame')

      register.style.display = "block"
      login.style.display = "block"
      tmovie.style.display = "none"
      logout.style.display = "none"
      tgame.style.display = "none"

      alert('Anda berhasil Log Out.')
    }
  

    return (
        <Router>
            <div className="navkiri">
                <i className="logo">MovieInventory.com</i>
                <span onClick={showMenu}><span className="menubar">☰</span></span>
            </div>
            <nav id="navigasi">
                <ul>
                    <li >
                        <Link to="/">Home</Link>
                    </li>
                    <li >
                        <Link to="/Pages/ListMovie">Movie</Link>
                    </li>
                    <li >
                        <Link to="/Pages/ListGame">Game</Link>
                    </li>
                    <li id="tmovie"> 
                        <Link to="/Pages/TabelMovie">T-Movie</Link>
                    </li>
                    <li id="tgame">  
                        <Link to="/Pages/TabelGame">T-Game</Link>
                    </li>
                </ul>    
                <div className="navkanan">
                    <ul>
                        <li id="register">
                            <Link to="/Pages/User">Register</Link>
                        </li>
                        <li id="login">
                            <Link to="/Pages/UserLogin">Log In</Link>
                        </li>
                        <li id="logout">
                            <Link onClick={handleLogout} to="/">Log Out</Link>
                        </li>
                    </ul>
                </div>
            </nav>

            <Switch>
                <Route exact path="/" component={Welcome} />
                <Route exact path="/Pages/ListMovie" component={ListMovie} />
                <Route path="/Pages/ListGame" component={ListGame}></Route>
                <Route path="/Pages/TabelMovie" component={TabelMovie}></Route>
                <Route path="/Pages/TabelGame" component={TabelGame}></Route>
                <Route exact path="/Pages/User" component={Register} />
                <Route exact path="/Pages/UserLogin" component={Login} />
            </Switch>
        </Router>
    )
}
export default Navbar