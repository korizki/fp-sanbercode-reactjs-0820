import React from 'react';
import '../style.css';
import ApartmentIcon from '@material-ui/icons/Apartment';
import RoomIcon from '@material-ui/icons/Room';
import CallIcon from '@material-ui/icons/Call';
import MailIcon from '@material-ui/icons/Mail';
import Button from '@material-ui/core/Button';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import InstagramIcon from '@material-ui/icons/Instagram';


function Footer (props) {
    return <div className="footer">
        <div className="fkonten">
            <div className="fisi">
                <div className="ftitle">About Us</div>
                <div className="fsubtitle">
                    <p><ApartmentIcon className="adjust"/> <b>PT. MGEntertainment</b></p>
                    <p><RoomIcon className="adjust"/>Ruko Palmerah Blok M Tanggerang </p>
                    <p style={{marginLeft: "30px", paddingTop: "15px"}}>Jakarta, Indonesia </p>
                    <p><CallIcon className="adjust"/>021 - 443879</p>
                    <p><MailIcon className="adjust"/>support@mgentertainment.com</p>
                </div>

            </div>
            <div className="fisi">
                <div className="ftitle">Social Media</div>
                <div className="fsubtitle">
                    <div className="tengah">
                        <Button className="full"
                        variant="contained"
                        color="primary"
                        startIcon={<FacebookIcon />}
                        > Follow Us On Facebook</Button>
                        <br/>
                        <br/>
                        <Button className="full"
                        variant="contained"
                        color="default"
                        startIcon={<TwitterIcon />}
                        > Follow Us On Twitter</Button>
                        <br/>
                        <br/>
                        <Button className="full"
                        variant="contained"
                        color="secondary"
                        startIcon={<InstagramIcon />}
                        > Follow Us On Instagram</Button>

                    </div>
                </div>
            </div>
            <div className="fisi">
                <div className="ftitle">Privacy &amp; Policy</div>
                <div className="fsubtitles">
                    <a href="www.google.com" ><p style={{fontSize: "1.3em"}}><b>Privacy</b> </p></a>
                    <a href="www.google.com" ><p style={{fontSize: "1.3em"}}><b>Terms and Conditions</b></p></a>
                </div>

            </div>
        </div>
    </div>
}

export default Footer