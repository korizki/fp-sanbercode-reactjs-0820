import React from "react";

import { Switch, Route } from "react-router";

const Routes = () => {
    function Home() {
        return <h2>Home</h2>;
      }
      
      function About() {
        return <h2>About</h2>;
      }
      
      function Dashboard() {
        return <h2>Users</h2>;
      }
      
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route path="/about">
        <About />
      </Route>
      <Route exact path="/dashboard">
        <Dashboard />
      </Route>
    </Switch>
  );
  
};



export default Routes;