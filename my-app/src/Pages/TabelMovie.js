import React, {useState, useEffect, useContext} from "react"
import axios from 'axios';
import '../style.css';
import Button from '@material-ui/core/Button'
import { UserContext } from "../User/user-context"
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'

const TabelMovie = () => {
    
    const [dataMovie, setDataMovie] = useState(null)
    const [user] = useContext(UserContext)
    const [input, setInput] = useState ({
        title: "",
        description: "",
        year: "",
        duration: "",
        genre: "",
        rating: "",
        review: "",
        image_url: ""
    })
    const [selectedId, setSelectedId] = useState(0)
    const [statusForm, setStatusForm] = useState("create")
    const [search, setSearch] = useState("")

    useEffect(() => {
        if (dataMovie === null){
            axios.get('https://backendexample.sanbersy.com/api/data-movie')
            .then (res => {
                setDataMovie(res.data.map(el => {
                    return {
                        id: el.id,
                        title: el.title,
                        description: el.description,
                        year: el.year,
                        duration: el.duration,
                        genre: el.genre,
                        rating: el.rating,
                        review: el.review,
                        image_url: el.image_url
                    }
                }))
            })
        }
    }, [dataMovie]);
    
    const changeInputName = (event) => {
        let typeOfInput = event.target.name

        switch (typeOfInput) {
            case "title" : {
                setInput({...input, title: event.target.value});
                break
            }
            case "description" : {
                setInput({...input, description: event.target.value});
                break
            }
            case "year" : {
                setInput({...input, year: event.target.value});
                break
            }
            case "duration" : {
                setInput({...input, duration: event.target.value});
                break
            }
            case "genre" : {
                setInput({...input, genre: event.target.value});
                break
            }
            case "rating" : {
                setInput({...input, rating: event.target.value});
                break
            }
            case "review" : {
                setInput({...input, review: event.target.value});
                break
            }
            case "image_url" : {
                setInput({...input, image_url: event.target.value});
                break
            }
            default:
                {break;}
        }
    }

    const submitForm = (event) => {
        event.preventDefault()

        let title = input.title
        console.log(input)

        if (title.replace(/\s/g, '') !== "") {
            if (statusForm === "create") {
                axios.post(`https://backendexample.sanbersy.com/api/data-movie`,
        
                {
                    title: input.title,
                    description: input.description,
                    year: input.year,
                    duration: input.duration,
                    genre: input.genre,
                    rating: input.rating,
                    review: input.review,
                    image_url: input.image_url
                },
                {
                    headers: {"Authorization" : `Bearer ${user.token}`}
                })
                .then (res => {
                    setDataMovie([...dataMovie, {id: res.data.id, ...input}])
                    alert('Data Movie berhasil ditambahkan!')
                    const showform = document.getElementById("forminput")
                    if (showform.style.display !== "block") {
                        showform.style.display = "block"
                    } else {
                        showform.style.display = "none"
                    }
                })
            } else if (statusForm === "edit") { 
                axios.put(`https://www.backendexample.sanbersy.com/api/data-movie/${selectedId}`,{
                    title: input.title,
                    description: input.description,
                    year: input.year,
                    duration: input.duration,
                    genre: input.genre,
                    rating: input.rating,
                    review: input.review,
                    image_url: input.image_url
                },
                {
                    headers: {"Authorization" : `Bearer ${user.token}`}
                })
                .then (res => {
                    let singleMovie = dataMovie.find(el => el.id === selectedId)
                    singleMovie.title = input.title
                    singleMovie.description = input.description
                    singleMovie.year = input.year
                    singleMovie.duration = input.duration
                    singleMovie.genre = input.genre
                    singleMovie.rating = input.rating
                    singleMovie.review = input.review
                    singleMovie.image_url = input.image_url
                    setDataMovie([...dataMovie])
                    alert('Data Movie berhasil diubah!')
                    const showform = document.getElementById("forminput")
                    if (showform.style.display !== "block") {
                        showform.style.display = "block"
                    } else {
                        showform.style.display = "none"
                    }
                })
                .catch(err => {
                    console.log(err.response.data)
                })
            }  
            setStatusForm ("create")
            setSelectedId(0)
            setInput({
                title: "",
                description: "",
                year: "",
                duration: "",
                genre: "",
                rating: 0,
                review: "",
                image_url: ""
            })  
        }
    }    

    const Action = ({itemId}) => {
        const handleDelete = () => {
            let newMovie = dataMovie.filter(el => el.id !== itemId)
            axios.delete(`https://backendexample.sanbersy.com/api/data-movie/${itemId}`, {headers: {"Authorization" : `Bearer ${user.token}`}} )
            .then (res => {
                console.log(res)
                alert('Data Movie berhasil dihapus')
            })
            setDataMovie([...newMovie])
        }

        const handleEdit = () => {
            let singleMovie = dataMovie.find(x => x.id === itemId)
            setInput({
                title: singleMovie.title,
                description: singleMovie.description,
                year: singleMovie.year,
                duration: singleMovie.duration,
                genre: singleMovie.genre,
                rating: singleMovie.rating,
                review: singleMovie.review,
                image_url: singleMovie.image_url
            }) 
            setSelectedId(itemId)
            setStatusForm("edit")
            const showform = document.getElementById("forminput")
            if (showform.style.display !== "block") {
                showform.style.display = "block"
            } else {
                showform.style.display = "none"
            }
        }
        return(
            <>
            <a class="inline" href="#forminput"><button className="btn edit" onClick={handleEdit}><EditIcon style={{width: "20px", height: "20px"}} /></button></a>
            &nbsp;
            <button className="btn delete" onClick={handleDelete}><DeleteIcon style={{width: "20px", height: "20px"}} /></button>
            </>
        )
    }
    function truncateString(str, num){
        if (str === null) {
            return ""
        } else {
            if (str.length <= num) {
                return str
            }
            return str.slice(0, num) + '...'
        }
    }

    const submitSearch = (e) => {
        e.preventDefault()

        axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
        .then(res => {
            let resDataMovie = res.data.map(el => {return {
                id: el.id,
                title: el.title,
                description: el.description,
                year: el.year,
                duration: el.duration,
                genre: el.genre,
                rating: el.rating,
                image_url: el.image_url
            }})
            let filteredDataMovie = resDataMovie.filter( x => x.title.toLowerCase().indexOf(search.toLowerCase()) !== -1)
            setDataMovie([...filteredDataMovie])
        })
    }

    const handleChangeSearch = (e) => {
        setSearch(e.target.value)
    }
    
    const showform = () => {
        var forminput = document.getElementById("forminput")
        if (forminput.style.display !== "block") {
            forminput.style.display = "block"
        } else {
            forminput.style.display = "none"
        }
    }

    const sortTitle = (e) => {    
        axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
        .then(res => {
            let resDataMovie = res.data.map(el => {return {
                id: el.id,
                title: el.title,
                description: el.description,
                year: el.year,
                duration: el.duration,
                genre: el.genre,
                rating: el.rating,
                image_url: el.image_url
            }})
            
            if (typeof(Storage) !== "undefined") {
                let localstorage = 0;
                if (localStorage.clickcount) {
                    localStorage.clickcount = 
                    localStorage.clickcount = parseInt(localStorage.clickcount) + 1
                } else {
                localStorage.clickcount = parseInt(localStorage.clickcount) + 1
                }
            }

            if (localStorage.clickcount % 2){
                let dataSortedIndex = resDataMovie.sort((a,b) => (a.title > b.title) - (a.title < b.title))            
                setDataMovie([...dataSortedIndex])
            } else {
                let dataSortedIndex = resDataMovie.sort((a,b) => (a.title < b.title) - (a.title > b.title))            
                setDataMovie([...dataSortedIndex])
            }
                
        })
    }

    // Sort Year
    const sortYear = (e) => {    
        axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
        .then(res => {
            let resDataMovie = res.data.map(el => {return {
                id: el.id,
                title: el.title,
                description: el.description,
                year: el.year,
                duration: el.duration,
                genre: el.genre,
                rating: el.rating,
                image_url: el.image_url
            }})
            
            if (typeof(Storage) !== "undefined") {
                let localstorage = 0;
                if (localStorage.clickcount) {
                    localStorage.clickcount = 
                    localStorage.clickcount = parseInt(localStorage.clickcount) + 1
                } else {
                localStorage.clickcount = parseInt(localStorage.clickcount) + 1
                }
            }

            if (localStorage.clickcount % 2){
                let dataSortedIndex = resDataMovie.sort((a,b) => (a.year > b.year) - (a.year < b.year))            
                setDataMovie([...dataSortedIndex])
            } else {
                let dataSortedIndex = resDataMovie.sort((a,b) => (a.year < b.year) - (a.year > b.year))            
                setDataMovie([...dataSortedIndex])
            }
                
        })
    }

    // Sort Duration
    const sortDuration = (e) => {    
        axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
        .then(res => {
            let resDataMovie = res.data.map(el => {return {
                id: el.id,
                title: el.title,
                description: el.description,
                year: el.year,
                duration: el.duration,
                genre: el.genre,
                rating: el.rating,
                image_url: el.image_url
            }})
            
            if (typeof(Storage) !== "undefined") {
                let localstorage = 0;
                if (localStorage.clickcount) {
                    localStorage.clickcount = 
                    localStorage.clickcount = parseInt(localStorage.clickcount) + 1
                } else {
                localStorage.clickcount = parseInt(localStorage.clickcount) + 1
                }
            }

            if (localStorage.clickcount % 2){
                let dataSortedIndex = resDataMovie.sort((a,b) => (a.duration > b.duration) - (a.duration < b.duration))            
                setDataMovie([...dataSortedIndex])
            } else {
                let dataSortedIndex = resDataMovie.sort((a,b) => (a.duration < b.duration) - (a.duration > b.duration))            
                setDataMovie([...dataSortedIndex])
            }
                
        })
    }
    // Sort Genre
    const sortGenre = (e) => {    
        axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
        .then(res => {
            let resDataMovie = res.data.map(el => {return {
                id: el.id,
                title: el.title,
                description: el.description,
                year: el.year,
                duration: el.duration,
                genre: el.genre,
                rating: el.rating,
                image_url: el.image_url
            }})
            
            if (typeof(Storage) !== "undefined") {
                let localstorage = 0;
                if (localStorage.clickcount) {
                    localStorage.clickcount = 
                    localStorage.clickcount = parseInt(localStorage.clickcount) + 1
                } else {
                localStorage.clickcount = parseInt(localStorage.clickcount) + 1
                }
            }

            if (localStorage.clickcount % 2){
                let dataSortedIndex = resDataMovie.sort((a,b) => (a.genre > b.genre) - (a.genre < b.genre))            
                setDataMovie([...dataSortedIndex])
            } else {
                let dataSortedIndex = resDataMovie.sort((a,b) => (a.genre < b.genre) - (a.genre > b.genre))            
                setDataMovie([...dataSortedIndex])
            }
                
        })
    }
    // Sort Rating
    const sortRating = (e) => {    
        axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
        .then(res => {
            let resDataMovie = res.data.map(el => {return {
                id: el.id,
                title: el.title,
                description: el.description,
                year: el.year,
                duration: el.duration,
                genre: el.genre,
                rating: el.rating,
                image_url: el.image_url
            }})
            
            if (typeof(Storage) !== "undefined") {
                let localstorage = 0;
                if (localStorage.clickcount) {
                    localStorage.clickcount = 
                    localStorage.clickcount = parseInt(localStorage.clickcount) + 1
                } else {
                localStorage.clickcount = parseInt(localStorage.clickcount) + 1
                }
            }

            if (localStorage.clickcount % 2){
                let dataSortedIndex = resDataMovie.sort((a,b) => (a.rating > b.rating) - (a.rating < b.rating))            
                setDataMovie([...dataSortedIndex])
            } else {
                let dataSortedIndex = resDataMovie.sort((a,b) => (a.rating < b.rating) - (a.rating > b.rating))            
                setDataMovie([...dataSortedIndex])
            }
                
        })
    }



    return (

    <>
    <div className="kontainer k4">
        <div className="wheader">
            <h1>Update data Film Favorit anda disini !</h1>
            <p> Kami menerima masukan dari anda, dan menghargai hal tersebut. 
                Telah tersedia menu input data Movie terbaru dibawah, yang nantinya dapat
                membantu reviewer dan tentunya dapat memudahkan mereka.
                <span className="br"></span>         
            </p> <br/>
            <p>
            <Button className="full"
            variant="contained"
            color="primary"
    
            > Mulai Berlangganan </Button>
            </p>
        </div>
    </div>

    <div className="formcari">
        <h2> Cari Data Movie </h2>
        <form className="cari" onSubmit = {submitSearch}>
            <input type="text" placeholder="Masukkan Judul Film disini . . ." value={search} onChange={handleChangeSearch} />
            <button>Search</button> 
        </form><a class="inline" onClick={showform} href="#forminput"><button class="tambah" >Tambah Movie</button></a>
    </div>
    <h1 className="titledm">Daftar Movie</h1>
    <table className="tabelMovie">
        <thead>
            <tr>
                <th >No</th>
                <th >Title <button style={{padding: "1px 5px", float: "right"}}onClick={sortTitle}>&#9662;</button> </th>  
                <th>Description </th>
                <th>Year <button style={{padding: "1px 5px", float: "right"}}onClick={sortYear}>&#9662;</button></th>
                <th>Duration <button style={{padding: "1px 5px", float: "right"}}onClick={sortDuration}>&#9662;</button></th>
                <th>Genre <button style={{padding: "1px 5px", float: "right"}}onClick={sortGenre}>&#9662;</button></th>
                <th>Rating <button style={{padding: "1px 5px", float: "right"}}onClick={sortRating}>&#9662;</button></th>
                <th>Action </th>
            </tr>
        </thead>
        <tbody>
            {
                dataMovie !== null && dataMovie.map((item, index)=>{
                return(
                    <tr key={index}>
                        <td>{index+1}</td>
                        <td><b>{item.title}</b></td>
                        <td title={item.description}> {truncateString(item.description, 20)}</td>
                        <td>{item.year}</td>
                        <td>{item.duration}</td>
                        <td>{item.genre}</td>
                        <td>{item.rating}</td>
                        <td>
                            <Action itemId={item.id} />
                        </td>
                    </tr>
                )})
            }

        </tbody>
    </table>

  

    {user !== null && (
        <section id="forminput" className = "boxregister inputform">
            <form style={{textAlign: "center"}} onSubmit={submitForm}>
                <div className="flexx">
                    <div className="flexchild2">
                        <label>Judul Film </label>
                        <input required type="text" name="title" value={input.title} onChange={changeInputName}/>
                        <label>Deskripsi </label>
                        <input required type="text" name="description" value={input.description} onChange={changeInputName}/>
                        <label>Tahun Release </label>
                        <input required type="text" name="year" value={input.year} onChange={changeInputName}/>
                        <label>Durasi (Menit) </label>
                        <input required type="text" name="duration" value={input.duration} onChange={changeInputName}/>
                    </div>
                    
                    <div className="flexchild">
                    <label>Genre Film (contoh: Action, Drama, Romance)</label>
                        <input required type="text" name="genre" value={input.genre} onChange={changeInputName}/>
                        <label>Rating Film </label>
                        <input required type="text" name="rating" value={input.rating} onChange={changeInputName}/>
                        <label>Review </label>
                        <input required type="text" name="review" value={input.review} onChange={changeInputName}/>
                        <label>Tautan Cover Film </label>
                        <input required type="text" name="image_url" value={input.image_url} onChange={changeInputName}/>
                    </div>    
                </div>
                <button>Save</button>
            </form>
        </section>
    )}
    </>
    )
}

export default TabelMovie