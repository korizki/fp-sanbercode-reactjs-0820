import React, {useState, useEffect, useContext} from "react"
import axios from 'axios';
import '../style.css';
import Button from '@material-ui/core/Button'
import { UserContext } from "../User/user-context"
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'


const TabelGame = () => {
    
    const [dataGame, setDataGame] = useState(null)
    const [user] = useContext(UserContext)
    const [input, setInput] = useState ({
        name: "",
        platform: "",
        release: "",
        genre: "",
        singlePlayer: "",
        multiplayer: "",
        image_url: ""
    })
    const [selectedId, setSelectedId] = useState(0)
    const [statusForm, setStatusForm] = useState("create")
    const [search, setSearch] = useState("")

    useEffect(() => {
        if (dataGame === null){
            axios.get('https://backendexample.sanbersy.com/api/data-game')
            .then (res => {
                setDataGame(res.data.map(el => {
                    return {
                        id: el.id,
                        name: el.name,
                        platform: el.platform,
                        release: el.release,
                        genre: el.genre,
                        singlePlayer: el.singlePlayer,
                        multiplayer: el.multiplayer,
                        image_url: el.image_url
                    }
                }))
            })
        }
    }, [dataGame]);
    
    const changeInputName = (event) => {
        let typeOfInput = event.target.name

        switch (typeOfInput) {
            case "name" : {
                setInput({...input, name: event.target.value});
                break
            }
            case "platform" : {
                setInput({...input, platform: event.target.value});
                break
            }
            case "release" : {
                setInput({...input, release: event.target.value});
                break
            }
            case "genre" : {
                setInput({...input, genre: event.target.value});
                break
            }
            case "singlePlayer" : {
                setInput({...input, singlePlayer: event.target.value});
                break
            }
            case "multiplayer" : {
                setInput({...input, multiplayer: event.target.value});
                break
            }
            case "image_url" : {
                setInput({...input, image_url: event.target.value});
                break
            }
            default:
                {break;}
        }
    }

    const submitForm = (event) => {
        event.preventDefault()

        let name = input.name
        console.log(input)

        if (name.replace(/\s/g, '') !== "") {
            if (statusForm === "create") {
                axios.post(`https://backendexample.sanbersy.com/api/data-game`,
        
                {
                    name: input.name,
                    platform: input.platform,
                    release: input.release,
                    duration: input.duration,
                    genre: input.genre,
                    singlePlayer: input.singlePlayer,
                    multiplayer: input.multiplayer,
                    image_url: input.image_url
                },
                {
                    headers: {"Authorization" : `Bearer ${user.token}`}
                })
                .then (res => {
                    setDataGame([...dataGame, {id: res.data.id, ...input}])
                    alert('Data Game berhasil ditambahkan!')
                    const showform = document.getElementById("forminput")
                    if (showform.style.display !== "block") {
                        showform.style.display = "block"
                    } else {
                        showform.style.display = "none"
                    }
                })
            } else if (statusForm === "edit") { 
                axios.put(`https://www.backendexample.sanbersy.com/api/data-game/${selectedId}`,{
                    name: input.name,
                    platform: input.platform,
                    release: input.release,
                    genre: input.genre,
                    singlePlayer: input.singlePlayer,
                    multiplayer: input.multiplayer,
                    image_url: input.image_url
                },
                {
                    headers: {"Authorization" : `Bearer ${user.token}`}
                })
                .then (res => {
                    let singleGame = dataGame.find(el => el.id === selectedId)
                    singleGame.name = input.name
                    singleGame.platform = input.platform
                    singleGame.release = input.release
                    singleGame.genre = input.genre
                    singleGame.singlePlayer = input.singlePlayer
                    singleGame.multiplayer = input.multiplayer
                    singleGame.image_url = input.image_url
                    setDataGame([...dataGame])
                    alert('Data Game berhasil diubah!')
                    const showform = document.getElementById("forminput")
                    if (showform.style.display !== "block") {
                        showform.style.display = "block"
                    } else {
                        showform.style.display = "none"
                    }
                })
                .catch(err => {
                    console.log(err.response.data)
                })
            }  
            setStatusForm ("create")
            setSelectedId(0)
            setInput({
                name: "",
                platform: "",
                release: "",
                genre: "",
                singlePlayer: 0,
                multiplayer: "",
                image_url: ""
            })  
        }
    }    

    const Action = ({itemId}) => {
        const handleDelete = () => {
            let newGame = dataGame.filter(el => el.id !== itemId)
            axios.delete(`https://backendexample.sanbersy.com/api/data-game/${itemId}`, {headers: {"Authorization" : `Bearer ${user.token}`}} )
            .then (res => {
                console.log(res)
                alert('Data Game berhasil dihapus')
            })
            setDataGame([...newGame])
        }

        const handleEdit = () => {
            let singleGame = dataGame.find(x => x.id === itemId)
            setInput({
                name: singleGame.name,
                platform: singleGame.platform,
                release: singleGame.release,
                genre: singleGame.genre,
                singlePlayer: singleGame.singlePlayer,
                multiplayer: singleGame.multiplayer,
                image_url: singleGame.image_url
            }) 
            setSelectedId(itemId)
            setStatusForm("edit")
            const showform = document.getElementById("forminput")
            if (showform.style.display !== "block") {
                showform.style.display = "block"
            } else {
                showform.style.display = "none"
            }
        }
        return(
            <>
            <a class="inline" href="#forminput"><button className="btn edit" onClick={handleEdit}><EditIcon style={{width: "20px", height: "20px"}} /></button></a>
            &nbsp;
            <button className="btn delete" onClick={handleDelete}><DeleteIcon style={{width: "20px", height: "20px"}} /></button>
            </>
        )
    }
    function truncateString(str, num){
        if (str === null) {
            return ""
        } else {
            if (str.length <= num) {
                return str
            }
            return str.slice(0, num) + '...'
        }
    }

    const submitSearch = (e) => {
        e.preventDefault()

        axios.get(`https://backendexample.sanbersy.com/api/data-game`)
        .then(res => {
            let resDataGame = res.data.map(el => {return {
                id: el.id,
                name: el.name,
                platform: el.platform,
                release: el.release,
                genre: el.genre,
                singlePlayer: el.singlePlayer,
                image_url: el.image_url
            }})
            let filteredDataGame = resDataGame.filter( x => x.name.toLowerCase().indexOf(search.toLowerCase()) !== -1)
            setDataGame([...filteredDataGame])
        })
    }

    const handleChangeSearch = (e) => {
        setSearch(e.target.value)
    }
    
    const showform = () => {
        var forminput = document.getElementById("forminput")
        if (forminput.style.display !== "block") {
            forminput.style.display = "block"
        } else {
            forminput.style.display = "none"
        }
    }

    const sortName = (e) => {    
        axios.get(`https://backendexample.sanbersy.com/api/data-game`)
        .then(res => {
            let resDataGame = res.data.map(el => {return {
                id: el.id,
                name: el.name,
                platform: el.platform,
                release: el.release,
                genre: el.genre,
                singlePlayer: el.singlePlayer,
                image_url: el.image_url
            }})
            
            if (typeof(Storage) !== "undefined") {
                let localstorage = 0;
                if (localStorage.clickcount) {
                    localStorage.clickcount = 
                    localStorage.clickcount = parseInt(localStorage.clickcount) + 1
                } else {
                localStorage.clickcount = parseInt(localStorage.clickcount) + 1
                }
            }

            if (localStorage.clickcount % 2){
                let dataSortedIndex = resDataGame.sort((a,b) => (a.name > b.name) - (a.name < b.name))            
                setDataGame([...dataSortedIndex])
            } else {
                let dataSortedIndex = resDataGame.sort((a,b) => (a.name < b.name) - (a.name > b.name))            
                setDataGame([...dataSortedIndex])
            }
                
        })
    }

    // Sort release
    const sortRelease = (e) => {    
        axios.get(`https://backendexample.sanbersy.com/api/data-game`)
        .then(res => {
            let resDataGame = res.data.map(el => {return {
                id: el.id,
                name: el.name,
                platform: el.platform,
                release: el.release,
                duration: el.duration,
                genre: el.genre,
                singlePlayer: el.singlePlayer,
                image_url: el.image_url
            }})
            
            if (typeof(Storage) !== "undefined") {
                let localstorage = 0;
                if (localStorage.clickcount) {
                    localStorage.clickcount = 
                    localStorage.clickcount = parseInt(localStorage.clickcount) + 1
                } else {
                localStorage.clickcount = parseInt(localStorage.clickcount) + 1
                }
            }

            if (localStorage.clickcount % 2){
                let dataSortedIndex = resDataGame.sort((a,b) => (a.release > b.release) - (a.release < b.release))            
                setDataGame([...dataSortedIndex])
            } else {
                let dataSortedIndex = resDataGame.sort((a,b) => (a.release < b.release) - (a.release > b.release))            
                setDataGame([...dataSortedIndex])
            }
                
        })
    }

    // Sort Genre
    const sortGenre = (e) => {    
        axios.get(`https://backendexample.sanbersy.com/api/data-Game`)
        .then(res => {
            let resDataGame = res.data.map(el => {return {
                id: el.id,
                name: el.name,
                platform: el.platform,
                release: el.release,
                duration: el.duration,
                genre: el.genre,
                singlePlayer: el.singlePlayer,
                image_url: el.image_url
            }})
            
            if (typeof(Storage) !== "undefined") {
                let localstorage = 0;
                if (localStorage.clickcount) {
                    localStorage.clickcount = 
                    localStorage.clickcount = parseInt(localStorage.clickcount) + 1
                } else {
                localStorage.clickcount = parseInt(localStorage.clickcount) + 1
                }
            }

            if (localStorage.clickcount % 2){
                let dataSortedIndex = resDataGame.sort((a,b) => (a.genre > b.genre) - (a.genre < b.genre))            
                setDataGame([...dataSortedIndex])
            } else {
                let dataSortedIndex = resDataGame.sort((a,b) => (a.genre < b.genre) - (a.genre > b.genre))            
                setDataGame([...dataSortedIndex])
            }
                
        })
    }
    // Sort singlePlayer
    const sortsinglePlayer = (e) => {    
        axios.get(`https://backendexample.sanbersy.com/api/data-Game`)
        .then(res => {
            let resDataGame = res.data.map(el => {return {
                id: el.id,
                name: el.name,
                platform: el.platform,
                release: el.release,
                duration: el.duration,
                genre: el.genre,
                singlePlayer: el.singlePlayer,
                image_url: el.image_url
            }})
            
            if (typeof(Storage) !== "undefined") {
                let localstorage = 0;
                if (localStorage.clickcount) {
                    localStorage.clickcount = 
                    localStorage.clickcount = parseInt(localStorage.clickcount) + 1
                } else {
                localStorage.clickcount = parseInt(localStorage.clickcount) + 1
                }
            }

            if (localStorage.clickcount % 2){
                let dataSortedIndex = resDataGame.sort((a,b) => (a.singlePlayer > b.singlePlayer) - (a.singlePlayer < b.singlePlayer))            
                setDataGame([...dataSortedIndex])
            } else {
                let dataSortedIndex = resDataGame.sort((a,b) => (a.singlePlayer < b.singlePlayer) - (a.singlePlayer > b.singlePlayer))            
                setDataGame([...dataSortedIndex])
            }
                
        })
    }
    // Sort Multi Player
    const sortmultiplayer = (e) => {    
        axios.get(`https://backendexample.sanbersy.com/api/data-Game`)
        .then(res => {
            let resDataGame = res.data.map(el => {return {
                id: el.id,
                name: el.name,
                platform: el.platform,
                release: el.release,
                duration: el.duration,
                genre: el.genre,
                singlePlayer: el.singlePlayer,
                image_url: el.image_url
            }})
            
            if (typeof(Storage) !== "undefined") {
                let localstorage = 0;
                if (localStorage.clickcount) {
                    localStorage.clickcount = 
                    localStorage.clickcount = parseInt(localStorage.clickcount) + 1
                } else {
                localStorage.clickcount = parseInt(localStorage.clickcount) + 1
                }
            }

            if (localStorage.clickcount % 2){
                let dataSortedIndex = resDataGame.sort((a,b) => (a.multiplayer > b.multiplayer) - (a.multiplayer < b.multiplayer))            
                setDataGame([...dataSortedIndex])
            } else {
                let dataSortedIndex = resDataGame.sort((a,b) => (a.multiplayer < b.multiplayer) - (a.multiplayer > b.multiplayer))            
                setDataGame([...dataSortedIndex])
            }
                
        })
    }



    return (

    <>
    <div className="kontainer k5 kiri5">
        <div className="wheader">
            <h1>Berbagai Game Favorit anda kini tersedia disini!</h1>
            <p style={{marginTop: "30px", fontSize: "0.9em"}}> Dapatkan berbagai Promo Spesial untuk anda, dan nikmati
                kemudahan akses berbagai Film terbaik Favoritmu.
                <br/>
                Berbagai ulasan dari Player lain yang memudahkan
                anda memilih Games yang sedang <i>Hits</i> saat ini, 
                tunggu apa lagi ! 
            </p> <br/>
            <p>
            <Button className="full"
            variant="contained"
            color="primary"
            //startIcon={<FacebookIcon />}
            > Mulai Berlangganan </Button>
            </p>
        </div>
    </div>

    <div className="formcari">
        <h2> Cari Data Game </h2>
        <form className="cari" onSubmit = {submitSearch}>
            <input type="text" placeholder="Masukkan Nama Game disini . . ." value={search} onChange={handleChangeSearch} />
            <button>Search</button> 
        </form><a class="inline" onClick={showform} href="#forminput"><button class="tambah" >Tambah Game</button></a>
    </div>
    <h1 className="titledm">Daftar Game</h1>
    <table className="tabelMovie">
        <thead>
            <tr>
                <th >No</th>
                <th >Game Name <button style={{padding: "1px 5px", float: "right"}}onClick={sortName}>&#9662;</button> </th>  
                <th>Platform </th>
                <th>Release <button style={{padding: "1px 5px", float: "right"}}onClick={sortRelease}>&#9662;</button></th>
                <th>Genre <button style={{padding: "1px 5px", float: "right"}}onClick={sortGenre}>&#9662;</button></th>
                <th>Single Player <button style={{padding: "1px 5px", float: "right"}}onClick={sortsinglePlayer}>&#9662;</button></th>
                <th>Multi Player <button style={{padding: "1px 5px", float: "right"}}onClick={sortmultiplayer}>&#9662;</button></th>
                
                <th>Action </th>
            </tr>
        </thead>
        <tbody>
            {
                dataGame !== null && dataGame.map((item, index)=>{
                return(
                    <tr key={index}>
                        <td>{index+1}</td>
                        <td><b>{item.name}</b></td>
                        <td name={item.platform}> {truncateString(item.platform, 20)}</td>
                        <td>{item.release}</td>
                        <td>{item.genre}</td>
                        <td>{item.singlePlayer}</td>
                        <td>{item.multiplayer}</td>
                        <td>
                            <Action itemId={item.id} />
                        </td>
                    </tr>
                )})
            }

        </tbody>
    </table>

    {user !== null && (
        <section id="forminput" className = "boxregister inputform">
            <form style={{textAlign: "center"}} onSubmit={submitForm}>
                <div className="flexx">
                    <div className="flexchild2">
                        <label>Nama Game </label>
                        <input required type="text" name="name" value={input.name} onChange={changeInputName}/>
                        <label>Platform </label>
                        <input required type="text" name="platform" value={input.platform} onChange={changeInputName}/>
                        <label>Tahun Release </label>
                        <input required type="text" name="release" value={input.release} onChange={changeInputName}/>
                        <label>Tautan Cover Game </label>
                        <input required type="text" name="image_url" value={input.image_url} onChange={changeInputName}/>
                    </div>
                    
                    <div className="flexchild">
                    <label>Genre Game (contoh: Action, Drama, Romance)</label>
                        <input required type="text" name="genre" value={input.genre} onChange={changeInputName}/>
                        <label>Single Player </label>
                        <input required type="text" name="singlePlayer" value={input.singlePlayer} onChange={changeInputName}/>
                        <label>Multi Player </label>
                        <input required type="text" name="multiplayer" value={input.multiplayer} onChange={changeInputName}/>
                        
                    </div>    
                </div>
                <button>Save</button>
            </form>
        </section>
    )}
    </>
    )
}

export default TabelGame