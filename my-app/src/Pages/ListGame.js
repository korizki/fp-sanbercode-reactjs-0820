import '../style.css';
import Button from '@material-ui/core/Button';
import React, {useState, useEffect} from "react";
import axios from 'axios';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline';

const ListGame = () =>{
    const [listGame, setListGame] = useState(null)
    const [input, setInput] = useState({ id: null, name: "", description: "", genre: "", review: "", image_url: ""})
  
    useEffect(() => {
      if (listGame === null){
        axios.get(`https://backendexample.sanbersy.com/api/data-game`)
        .then(res => {
          setListGame(res.data)
          console.log(res.data)
        })
      }
    }, [listGame]);
  
  
    const submitForm = (event) =>{
      event.preventDefault()
      
      if ( input.id === null){
        axios.post(`https://backendexample.sanbersy.com/api/data-game`, { name: input.name})
        .then(res => {
          var data = res.data
          setListGame([...listGame, {id: data.id, name: data.name,
            description: data.description, genre: data.genre,
            review: data.review, image_url: data.image_url 
        }])
          setInput({ id: null, name: "", description: "", genre: "", review: "", image_url: ""})
        })
  
      }else{
  
        axios.put(`https://backendexample.sanbersy.com/api/data-game/${input.id}`, { name: input.name})
        .then(res => {
          var newListGame = listGame.map(x => {
            if (x.id === input.id){
              x.name = input.name
            }
            return x
          })
          setListGame(newListGame)
          setInput({ id: null, name: "", description: "", genre: "", review: "", image_url: ""})
        })
      }
    }
  
    const deleteListGame = (event) =>{
      var idGame= parseInt(event.target.value) 
      axios.delete(`https://backendexample.sanbersy.com/api/data-game/${idGame}`)
      .then(res => {
        var newListGame = listGame.filter(x=> x.id !== idGame)
        setListGame(newListGame)
      })
    }
  
  
    const changeInputName = (event) =>{
      var value= event.target.value
      setInput({...input, name: value})
    }
  
    const editForm = (event) =>{
      var idGame= parseInt(event.target.value)
      var game = listGame.find(x=> x.id === idGame)
  
      setInput({id: idGame, name: game.name})
  
    }
  
    return(
      <> <div className="kontainer k3 kiri" >
          <div className="wheader">
            <h1>Temukan Game ter-Update dan Dapatkan Info Menarik !</h1>
            <p> Kami menyediakan ulasan / review Game terupdate sehingga
            memudahkan anda untuk memilih game yang ingin anda maunkan. Atau anda
            bisa lihat pada menu Favorit.
                <span className="br"></span>
                 
            </p> <br/>
            <p>
            <Button className="full"
            variant="contained"
            color="primary"
            //startIcon={<FacebookIcon />}
            > Mulai Berlangganan </Button>
            </p>
        </div>

        
        
        </div>
        <div className="baris">
            <h1 className="lmtitle">Daftar Game</h1>
            <div className="isifilm">
            {
                listGame !== null && listGame.map((item, index)=>{
                return(
                    <div className="kotak">
                        <img alt="MovieImage" className="kontens" src={item.image_url}></img>
                        <div className="judul">{item.name}</div>
                        <div className="desc">
                            {item.platform}
                        </div><hr/>
                        <div className="desc des">
                            {item.genre} | {item.release}  
                        </div>
                        <button className="live"><PlayCircleOutlineIcon className="pas"/>Simak Review</button>
                        <div className="btn" >
                          <Button variant="contained" startIcon={<EditIcon />} color="primary" value={item.id} style={{marginRight: "5px"}} onClick={editForm}><span className="hapus">Edit</span> </Button>
                          <Button variant="contained" startIcon={<DeleteIcon />} color="secondary" value={item.id} onClick={deleteListGame}> <span className="hapus">Hapus</span> </Button>
                        </div>
                    </div>
                )
                })
            }
            </div>
        </div>
      </>
    )
  }
  
  export default ListGame
  