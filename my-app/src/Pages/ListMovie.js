import '../style.css';
import Button from '@material-ui/core/Button';
import React, {useState, useEffect} from "react";
import axios from 'axios';
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline';

const ListMovie = () =>{
    const [listMovie, setListMovie] = useState(null)

    useEffect(() => {
      if (listMovie === null){
        axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
        .then(res => {
          setListMovie(res.data)
          console.log(res.data)
        })
      }
    }, [listMovie]);
  
    return(
      <> <div className="kontainer k2" >
          <div className="wheader">
            <h1>Temukan Film dengan Rating Terbaik dan Update !</h1>
            <p> Anda dapat dengan mudah menemukan
            berbagai film dengan hanya melakukan pencarian. Atau anda juga bisa memilih
            pada menu Favorit yang telah kami siapkan untuk anda.
                <span className="br"></span>
                 
            </p> <br/>
            <p>
            <Button className="full"
            variant="contained"
            color="primary"
            //startIcon={<FacebookIcon />}
            > Mulai Berlangganan </Button>
            </p>
        </div>

        
        
        </div>
        <div className="baris">
            <h1 className="lmtitle">Daftar Movie</h1>
            <div className="isifilm">
            {
                listMovie !== null && listMovie.map((item, index)=>{
                return(
                    <div className="kotak">
                        <img alt="MovieImage" className="kontens" src={item.image_url}></img>
                        <div className="judul">{item.title}</div>
                        <div className="desc">
                            {item.genre}
                        </div><hr/>
                        <div className="desc des">
                            {item.rating} | {item.duration} minutes | {item.year}  
                        </div>
                        <button className="live"><PlayCircleOutlineIcon className="pas"/>Live Streaming</button>
                    </div>
                )
                })
            }
            </div>
        </div>
      </>
    )
  }
  
  export default ListMovie
  