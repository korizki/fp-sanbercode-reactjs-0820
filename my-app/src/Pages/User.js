import React, { useContext, useState } from "react"
import {UserContext} from "../User/user-context"
import axios from "axios"
import {useHistory} from "react-router-dom"
import AssignmentIndIcon from "@material-ui/icons/AssignmentInd"

const Register = () =>{
  const [, setUser] = useContext(UserContext)
  const [input, setInput] = useState({name: "", email: "" , password: ""})
  let history = useHistory()
  const handleSubmit = (event) =>{
    event.preventDefault()
    axios.post("https://backendexample.sanbersy.com/api/register", {
      name: input.name, 
      email: input.email, 
      password: input.password
    }).then(
      (res)=>{
        console.log(res)
        var user = res.data.user
        var token = res.data.token
        var currentUser = {name: user.name, email: user.email, token }
        setUser(currentUser)
        localStorage.setItem("user", JSON.stringify(currentUser))
        alert(`Pendaftaran berhasil, Silahkan Log In `)
        history.push("/Pages/UserLogin")
        setInput({name: "", email: "", password: ""})
      }
    ).catch((err)=>{
      alert(err)
    })
  }

  const handleChange = (event) =>{
    let value = event.target.value
    let name = event.target.name
    switch (name){
      case "name":{
        setInput({...input, name: value})
        break;
      }
      case "email":{
        setInput({...input, email: value})
        break;
      }
      case "password":{
        setInput({...input, password: value})
        break;
      }
      default:{break;}
    }
  }


  return(
    <>
    <div className="kontainer">
        <div className="wheader">
            <h3>Form Register User</h3>
            <div className="boxregister">
                <form onSubmit={handleSubmit}>
                <label>Nama Lengkap </label><br/>
                <input type="text" name="name" onChange={handleChange} value={input.name}/>
                <br/>
                <label>Email </label><br/>
                <input type="email" name="email" onChange={handleChange} value={input.email}/>
                <br/>
                <label>Password </label><br/>
                <input type="password" name="password" onChange={handleChange} value={input.password}/>
                <br/>
                <button><AssignmentIndIcon style={{float: "left"}}/>Register</button>
                </form>
            </div>
        </div>
    </div>
    </>
  )
}

export default Register
   
    