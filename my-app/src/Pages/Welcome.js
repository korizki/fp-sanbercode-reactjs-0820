import React from 'react';
import '../style.css';
import Button from '@material-ui/core/Button'


function Welcome (props) {
    return (
    <>
    <div className="kontainer">
        <div className="wheader">
            <h1 >Berbagai Film Favorit anda kini tersedia disini!</h1>
            <p > Dapatkan berbagai Promo Spesial untuk anda, dan nikmati
                kemudahan akses berbagai Film terbaik Favoritmu.
                <span className="br"></span>
                Berbagai ulasan dari penonton yang lain yang memudahkan
                anda memilih Film dan Games yang sedang <i>Hits</i> saat ini, 
                tunggu apa lagi ! 
            </p> <br/>
            <p>
            <Button className="full"
            variant="contained"
            color="primary"
            //startIcon={<FacebookIcon />}
            > Mulai Berlangganan </Button>
            </p>
        </div>
    </div>
    </>
    )
}

export default Welcome