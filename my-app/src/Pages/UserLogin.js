import React, { useContext, useState } from "react"
import {UserContext} from "../User/user-context"
import axios from "axios"
import {useHistory} from "react-router-dom"
import ExitToAppIcon from "@material-ui/icons/ExitToApp"

const Login = () =>{
    let history = useHistory()
    const [, setUser] = useContext(UserContext)
    const [input, setInput] = useState({email: "" , password: ""})
  
    const handleSubmit = (event) =>{
      event.preventDefault()
      axios.post("https://backendexample.sanbersy.com/api/user-login", {
        email: input.email, 
        password: input.password
      }).then(
        (res)=>{
          var user = res.data.user
          var token = res.data.token
          var currentUser = {name: user.name, email: user.email, token }
          setUser(currentUser)
          localStorage.setItem("user", JSON.stringify(currentUser))
          alert(`Anda berhasil Log In, Selamat Datang ${user.name}`)
          
          var login = document.getElementById("login")
          var logout = document.getElementById("logout")
          var register = document.getElementById("register")
          var tmovie = document.getElementById("tmovie")
          var tgame = document.getElementById("tgame")
          
          login.style.display="none"
          logout.style.display="block"
          register.style.display="none"
          tmovie.style.display="block"
          tgame.style.display="block"

          history.push("/Pages/TabelMovie")
        }
      ).catch((err)=>{
        alert(err)
      })
    }
  

  const handleChange = (event) =>{
    let value = event.target.value
    let name = event.target.name
    switch (name){
      case "email":{
        setInput({...input, email: value})
        break;
      }
      case "password":{
        setInput({...input, password: value})
        break;
      }
      default:{break;}
    }
  }

  return(
    <>
      <div className="kontainer">
        <div className="wheader">
            <h3>Form Log In User</h3>
            <div className="boxregister">
                <form onSubmit={handleSubmit}>
                <label>Email: </label>
                <input type="email" name="email" onChange={handleChange} value={input.email}/>
                <br/>
                <label>Password: </label>
                <input type="password" name="password" onChange={handleChange} value={input.password}/>
                <br/>
                <button><ExitToAppIcon style={{float: "left"}}/>Login</button>
                </form>
            </div>
        </div>
      </div>
    </>
  )
}

export default Login